# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Rmk.Repo.insert!(%Rmk.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.



#Rmk.Repo.insert!(%Rmk.Sensor.Device{name: "ratva-2", serialnumber: "U1292", longitude: 27.16793793, latitude: 59.18957507})
Rmk.DatabaseSeeder.insert_data()