defmodule Rmk.Repo.Migrations.CreateDevices do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :name, :string
      add :serialnumber, :string
      add :lat, :float
      add :lng, :float

      timestamps()
    end

  end
end
