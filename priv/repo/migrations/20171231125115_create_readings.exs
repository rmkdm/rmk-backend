defmodule Rmk.Repo.Migrations.CreateReadings do
  use Ecto.Migration

  def change do
    create table(:readings) do
      add :timestamp, :naive_datetime
      add :water_level, :float
      add :water_temperature, :float
      add :device_id, references(:devices, on_delete: :nothing)

      timestamps()
    end

    create index(:readings, [:device_id])
  end
end
