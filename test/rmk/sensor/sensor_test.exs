defmodule Rmk.SensorTest do
  use Rmk.DataCase

  alias Rmk.Sensor

  describe "devices" do
    alias Rmk.Sensor.Device

    @valid_attrs %{lat: 120.5, lng: 120.5, name: "some name", serialnumber: "some serialnumber"}
    @update_attrs %{lat: 456.7, lng: 456.7, name: "some updated name", serialnumber: "some updated serialnumber"}
    @invalid_attrs %{lat: nil, lng: nil, name: nil, serialnumber: nil}

    def device_fixture(attrs \\ %{}) do
      {:ok, device} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Sensor.create_device()

      device
    end

    test "list_devices/0 returns all devices" do
      device = device_fixture()
      assert Sensor.list_devices() == [device]
    end

    test "get_device!/1 returns the device with given id" do
      device = device_fixture()
      assert Sensor.get_device!(device.id) == device
    end

    test "create_device/1 with valid data creates a device" do
      assert {:ok, %Device{} = device} = Sensor.create_device(@valid_attrs)
      assert device.lat == 120.5
      assert device.lng == 120.5
      assert device.name == "some name"
      assert device.serialnumber == "some serialnumber"
    end

    test "create_device/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sensor.create_device(@invalid_attrs)
    end

    test "update_device/2 with valid data updates the device" do
      device = device_fixture()
      assert {:ok, device} = Sensor.update_device(device, @update_attrs)
      assert %Device{} = device
      assert device.lat == 456.7
      assert device.lng == 456.7
      assert device.name == "some updated name"
      assert device.serialnumber == "some updated serialnumber"
    end

    test "update_device/2 with invalid data returns error changeset" do
      device = device_fixture()
      assert {:error, %Ecto.Changeset{}} = Sensor.update_device(device, @invalid_attrs)
      assert device == Sensor.get_device!(device.id)
    end

    test "delete_device/1 deletes the device" do
      device = device_fixture()
      assert {:ok, %Device{}} = Sensor.delete_device(device)
      assert_raise Ecto.NoResultsError, fn -> Sensor.get_device!(device.id) end
    end

    test "change_device/1 returns a device changeset" do
      device = device_fixture()
      assert %Ecto.Changeset{} = Sensor.change_device(device)
    end
  end

  describe "readings" do
    alias Rmk.Sensor.Reading

    @valid_attrs %{timestamp: ~N[2010-04-17 14:00:00.000000], water_level: 120.5, water_temperature: 120.5}
    @update_attrs %{timestamp: ~N[2011-05-18 15:01:01.000000], water_level: 456.7, water_temperature: 456.7}
    @invalid_attrs %{timestamp: nil, water_level: nil, water_temperature: nil}

    def reading_fixture(attrs \\ %{}) do
      {:ok, reading} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Sensor.create_reading()

      reading
    end

    test "list_readings/0 returns all readings" do
      reading = reading_fixture()
      assert Sensor.list_readings() == [reading]
    end

    test "get_reading!/1 returns the reading with given id" do
      reading = reading_fixture()
      assert Sensor.get_reading!(reading.id) == reading
    end

    test "create_reading/1 with valid data creates a reading" do
      assert {:ok, %Reading{} = reading} = Sensor.create_reading(@valid_attrs)
      assert reading.timestamp == ~N[2010-04-17 14:00:00.000000]
      assert reading.water_level == 120.5
      assert reading.water_temperature == 120.5
    end

    test "create_reading/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sensor.create_reading(@invalid_attrs)
    end

    test "update_reading/2 with valid data updates the reading" do
      reading = reading_fixture()
      assert {:ok, reading} = Sensor.update_reading(reading, @update_attrs)
      assert %Reading{} = reading
      assert reading.timestamp == ~N[2011-05-18 15:01:01.000000]
      assert reading.water_level == 456.7
      assert reading.water_temperature == 456.7
    end

    test "update_reading/2 with invalid data returns error changeset" do
      reading = reading_fixture()
      assert {:error, %Ecto.Changeset{}} = Sensor.update_reading(reading, @invalid_attrs)
      assert reading == Sensor.get_reading!(reading.id)
    end

    test "delete_reading/1 deletes the reading" do
      reading = reading_fixture()
      assert {:ok, %Reading{}} = Sensor.delete_reading(reading)
      assert_raise Ecto.NoResultsError, fn -> Sensor.get_reading!(reading.id) end
    end

    test "change_reading/1 returns a reading changeset" do
      reading = reading_fixture()
      assert %Ecto.Changeset{} = Sensor.change_reading(reading)
    end
  end
end
