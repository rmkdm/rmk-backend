# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :rmk,
  ecto_repos: [Rmk.Repo]

# Configures the endpoint
config :rmk, RmkWeb.Endpoint,
  url: [host: "localhost"],
  http: [compress: true],
  secret_key_base: "GN2Yd10mbKGwVF+BOab35vwSh4rHzb4NKM5iEQmJVngV8+J+eXrpOr3hAJsm/vfO",
  render_errors: [view: RmkWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Rmk.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :rmk, Rmk.Repo,
  ownership_timeout: 60_000,
  timeout: 450000
# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
