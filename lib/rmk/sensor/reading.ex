defmodule Rmk.Sensor.Reading do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rmk.Sensor.Reading
  alias Rmk.Sensor.Device

  schema "readings" do
    field :timestamp, :naive_datetime
    field :water_level, :float
    field :water_temperature, :float
    belongs_to :device, Device

    timestamps()
  end

  @doc false
  def changeset(%Reading{} = reading, attrs) do
    reading
    |> cast(attrs, [:timestamp, :water_level, :water_temperature, :device_id])
    |> validate_required([:timestamp, :water_level, :water_temperature])
    |> assoc_constraint(:device)
  end
end
