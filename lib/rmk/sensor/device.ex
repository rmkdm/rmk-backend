defmodule Rmk.Sensor.Device do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rmk.Sensor.Device
  alias Rmk.Sensor.Reading

  schema "devices" do
    field :lat, :float
    field :lng, :float
    field :name, :string
    field :serialnumber, :string
    has_many :readings, Reading

    timestamps()
  end

  @doc false
  def changeset(%Device{} = device, attrs) do
    device
    |> cast(attrs, [:name, :serialnumber, :lat, :lng])
    |> cast_assoc(:readings)
    |> validate_required([:name, :serialnumber, :lat, :lng])
  end
end
