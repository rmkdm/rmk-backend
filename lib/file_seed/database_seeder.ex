defmodule Rmk.DatabaseSeeder do
  alias Rmk.Repo
  alias Rmk.Sensor.Device
  alias Rmk.Sensor.Reading

  def insert_devices() do
    {:ok, sensor_body} = File.read("lib/file_seed/sensors.json")
    {:ok, sensor_json} = Poison.decode(sensor_body)

    sensor_json
      |> Enum.map(fn item -> serialnumber_type(item) end)
      |> Enum.map(fn item -> Device.changeset(%Device{}, item) end)
      |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
  end

  def serialnumber_type(item) do
    cond do
      is_integer(item["serialnumber"]) -> Map.put(item, "serialnumber", Integer.to_string(item["serialnumber"]))
      true -> item
    end
  end
  def insert_data() do
    {:ok, body} = File.read("lib/file_seed/data.json")
    {:ok, json} = Poison.decode(body)

    json
      |> Enum.each(fn {device_name, data} ->
        data = Map.put(data, "readings", data["readings"] |> Enum.filter(fn reading -> reading["water_level"] != "NaN" end))
        device = data |> Map.put("name", device_name)
        %Device{}
          |> Device.changeset(device)
          |> Repo.insert()
      end)
  end
end
