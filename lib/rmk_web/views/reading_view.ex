defmodule RmkWeb.ReadingView do
  use RmkWeb, :view
  alias RmkWeb.ReadingView

  def render("index.json", %{readings: readings}) do
    %{data: render_many(readings, ReadingView, "reading.json")}
  end

  def render("show.json", %{reading: reading}) do
    %{data: render_one(reading, ReadingView, "reading.json")}
  end

  def render("reading.json", %{reading: reading}) do
    %{id: reading.id,
      timestamp: reading.timestamp,
      water_level: reading.water_level,
      water_temperature: reading.water_temperature}
  end

  def render("reading-simple.json", %{reading: reading}) do
    %{timestamp: NaiveDateTime.to_iso8601(reading.timestamp),
      water_level: reading.water_level,
      water_temperature: reading.water_temperature}
  end
end
