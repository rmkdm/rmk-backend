defmodule RmkWeb.DeviceView do
  use RmkWeb, :view
  alias RmkWeb.DeviceView
  alias RmkWeb.ReadingView

  def render("index.json", %{devices: devices}) do
    %{data: render_many(devices, DeviceView, "device.json")}
  end

  def render("show.json", %{device: device}) do
    %{data: render_one(device, DeviceView, "device.json")}
  end

  def render("device.json", %{device: device}) do
    if (Ecto.assoc_loaded?(device.readings)) do
      %{id: device.id,
        name: device.name,
        serialnumber: device.serialnumber,
        latitude: device.lat,
        longitude: device.lng,
        readings: render_many(device.readings, ReadingView, "reading-simple.json")
      }
    else
      %{id: device.id,
        name: device.name,
        serialnumber: device.serialnumber,
        latitude: device.lat,
        longitude: device.lng,
      }
    end
  end
end
