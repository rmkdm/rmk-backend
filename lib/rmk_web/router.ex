defmodule RmkWeb.Router do
  use RmkWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", RmkWeb do
    pipe_through :api
    resources "/devices", DeviceController, except: [:new, :edit]
    resources "/readings", ReadingController, except: [:new, :edit]

  end
end
