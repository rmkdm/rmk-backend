defmodule RmkWeb.DeviceController do
  use RmkWeb, :controller

  alias Rmk.Sensor
  alias Rmk.Sensor.Device

  action_fallback RmkWeb.FallbackController

  def index(conn, _params) do
    devices = Sensor.list_devices()
    render(conn, "index.json", devices: devices)
  end

  def create(conn, %{"device" => device_params}) do
    with {:ok, %Device{} = device} <- Sensor.create_device(device_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", device_path(conn, :show, device))
      |> render("show.json", device: device)
    end
  end

  def show(conn, %{"id" => id}) do
    device = Sensor.get_device!(id)
    render(conn, "show.json", device: device)
  end

  def update(conn, %{"id" => id, "device" => device_params}) do
    device = Sensor.get_device!(id)

    with {:ok, %Device{} = device} <- Sensor.update_device(device, device_params) do
      render(conn, "show.json", device: device)
    end
  end

  def delete(conn, %{"id" => id}) do
    device = Sensor.get_device!(id)
    with {:ok, %Device{}} <- Sensor.delete_device(device) do
      send_resp(conn, :no_content, "")
    end
  end
end
